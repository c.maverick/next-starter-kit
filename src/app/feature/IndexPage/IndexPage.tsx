import React, { FC } from 'react';

import { FormExample } from '@/app/ui/FormExample';

import styles from './IndexPage.module.scss';

export const IndexPage: FC = () => {
  return (
    <div className={styles.root}>
      <h1>Заголовок</h1>
      <p>Описание</p>
      <FormExample />
    </div>
  );
};
