import { RefObject, useCallback, useRef } from 'react';
import { SwiperRef } from 'swiper/react';

export default function useSwiperControl(): [
  sliderRef: RefObject<SwiperRef>,
  handlePrev: () => void,
  handleNext: () => void
] {
  const sliderRef = useRef<SwiperRef>(null);

  const handlePrev = useCallback(() => {
    if (sliderRef.current) {
      sliderRef.current.swiper.slidePrev();
    }
  }, []);

  const handleNext = useCallback(() => {
    if (sliderRef.current) {
      sliderRef.current.swiper.slideNext();
    }
  }, []);

  return [sliderRef, handlePrev, handleNext];
}
