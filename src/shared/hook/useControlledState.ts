import { Dispatch, SetStateAction, useState } from 'react';

export default function useControlledState<S>(value: S): [S, Dispatch<SetStateAction<S>>] {
  const [prevValue, setPrevValue] = useState<S>(value);
  const [stateValue, setStateValue] = useState<S>(value);

  // Обновляем состояние, если изменился проп (контролируемый компонент)
  if (prevValue !== value) {
    setStateValue(value);
    setPrevValue(value);
  }

  return [stateValue, setStateValue];
}
