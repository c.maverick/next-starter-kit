import { RefObject, useEffect } from 'react';

import { useLatest } from './useLatest';

interface UseOutsideClick {
  elementRef: RefObject<HTMLDivElement>;
  handler: () => void;
  attached: boolean;
}

export const useOutsideClick = ({ elementRef, handler, attached }: UseOutsideClick) => {
  const latestHandler = useLatest(handler);

  useEffect(() => {
    if (!attached) return;

    //TODO: ts
    const handleClick = (e: any) => {
      if (!elementRef.current) return;

      if (!elementRef.current.contains(e.target)) {
        latestHandler.current();
      }
    };

    document.addEventListener('click', handleClick);

    return () => {
      document.removeEventListener('click', handleClick);
    };
  }, [elementRef, latestHandler, attached]);
};
