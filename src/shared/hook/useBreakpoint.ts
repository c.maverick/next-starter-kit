import { useEffect, useState } from 'react';

export const useMedia = (query: string, defaultState: boolean = false) => {
  const [state, setState] = useState(defaultState);

  useEffect(() => {
    let mounted = true;
    const mql = window.matchMedia(query);

    const onChange = () => {
      if (!mounted) {
        return;
      }
      setState(mql.matches);
    };

    if (mql.addEventListener) {
      mql.addEventListener('change', onChange);
    } else {
      mql.addListener(onChange);
    }
    setState(mql.matches);

    return () => {
      mounted = false;
      if (mql.removeEventListener) {
        mql.removeEventListener('change', onChange);
      } else {
        mql.removeListener(onChange);
      }
      setState(mql.matches);
    };
  }, [query]);

  return state;
};

export type Breakpoint = 'mobile' | 'mobileM' | 'tablet' | 'desktop';
export type BreakpointType = 'max' | 'min';
export type BreakpointWithDesktopWide = Breakpoint | 'desktopWide';

export const breakpoints: Record<Breakpoint, number> = {
  mobile: 640,
  mobileM: 800,
  tablet: 1024,
  desktop: 1440,
};

export const useBreakpoint = (
  breakpoint: Breakpoint,
  type: BreakpointType = 'max',
  defaultState: boolean = false
) => useMedia(`(${type}-width: ${breakpoints[breakpoint] - 0.1}px)`, defaultState);

export const useCurrentBreakpoint = (): BreakpointWithDesktopWide => {
  const isMobile = useBreakpoint('mobile');
  const isMobileM = useBreakpoint('mobileM');
  const isTablet = useBreakpoint('tablet');
  const isDesktop = useBreakpoint('desktop');

  if (isMobile) {
    return 'mobile';
  }

  if (isMobileM) {
    return 'mobileM';
  }

  if (isTablet) {
    return 'tablet';
  }

  return 'desktop';
};

export const useTablet = (defaultState?: boolean) => useBreakpoint('tablet', 'max', defaultState);
export const useMobile = (defaultState?: boolean) => useBreakpoint('mobile', 'max', defaultState);
export const useMobileM = (defaultState?: boolean) => useBreakpoint('mobileM', 'max', defaultState);
